 /*connection $psql postgres
 \c timetable #to get connected to database
 
 */
ALTER USER vinay WITH PASSWORD 'vinay vinu';

create user express with  password 'vinay vinu';
grant all privileges on database timetable to express;

CREATE TABLE MISC_START_DATE(
    START_DATE DATE
);
INSERT INTO MISC_START_DATE VALUES ('20-10-2021');

/*
CREATE TABLE SAMP(
    NAME VARCHAR(20)
);

INSERT INTO SAMP(NAME) VALUES('VINAY');
INSERT INTO SAMP(NAME) VALUES('ABC');
INSERT INTO SAMP VALUES('MAHESH');

*/
CREATE TABLE PERMISSION(
    permission_id integer,
    permission_name VARCHAR(20),
    primary key(permission_id)
);


CREATE TABLE ROLES(
    role_id integer,
    role_name varchar(20),
    permission integer[10],
    primary key(role_id)
);


CREATE TABLE USERS(
    user_id VARCHAR(50),
    password  VARCHAR(250),
    role_id  integer,
    primary key(user_id),
    foreign key(role_id) references ROLES(role_id) ON DELETE CASCADE
);

alter table users add column user_name varchar(100);

CREATE TABLE CLASS(
    CLASS_ID numeric(10), /* Generate Automatically */
    CLASS_NAME varchar(50),
    class_strength int,
    NO_OF_SUBJECTS integer, /*auto Calculate*/
    NO_OF_HOURS_PER_WEEK integer, /* auto calculate */
    primary key(class_id)    
);

ALTER TABLE CLASS ADD  UNIQUE (CLASS_NAME);

CREATE TABLE TEACHERS(
    TID  numeric(10),
    NAME VARCHAR(50),
    PHONE NUMERIC(10),
    EMAIL VARCHAR(50),
    NO_OF_HOURS_PER_WEEK INTEGER,
    authorized_lectures numeric(10),
    PRIMARY KEY(TID)
);

ALTER TABLE TEACHERS ADD  UNIQUE (PHONE);
ALTER TABLE TEACHERS ADD  UNIQUE (EMAIL);

CREATE TABLE ROOM(
    ROOM_NO NUMERIC(10),
    ROOM_NAME VARCHAR(20),
    PRIMARY KEY(ROOM_NO)
);

CREATE TABLE SUBJECTS(
    SUBJECT_ID VARCHAR(10),
    SUBJECT_NAME VARCHAR(50),
    CLASS_ID    NUMERIC(10),
    TEACHER_ID NUMERIC(10),
    ROOM_ID NUMERIC(10),
    SUBJECT_TYPE VARCHAR(50),
    DURATION_OF_ONE_CLASS DECIMAL(2,2),
    NO_OF_CLASSES_PER_WEEK NUMERIC(10),
    PRIMARY KEY(SUBJECT_ID),
    FOREIGN KEY(CLASS_ID) REFERENCES CLASS(CLASS_ID),
    FOREIGN KEY(TEACHER_ID) REFERENCES TEACHERS(TID),
    FOREIGN KEY(ROOM_ID) REFERENCES ROOM(ROOM_NO)
);

CREATE TABLE TIMETABLE(
    TIMETABLE_ID NUMERIC(10),
    GEN_DATE DATE,
    GEN_TIME TIME,
    PRIMARY KEY(TIMETABLE_ID)
);

CREATE TABLE LECTURES(
    LECTURE_ID integer GENERATED ALWAYS AS IDENTITY,
    START_TIME TIME,
    END_TIME TIME,
    SUBJECT_ID VARCHAR(10),
    TIMETABLE_ID NUMERIC(10),
    DAY_NAME varchar(10),
    TEACHER_id NUMERIC(10),
    ROOM_ID NUMERIC(10),
    CLASS_ID NUMERIC(10),
    PRIMARY KEY(LECTURE_ID),        
    FOREIGN KEY(SUBJECT_ID) REFERENCES SUBJECTS(SUBJECT_ID),
    FOREIGN KEY(TIMETABLE_ID) REFERENCES TIMETABLE(TIMETABLE_ID)
);

INSERT INTO PERMISSION(permission_id,permission_name) VALUES(1,'WRITE'); /*  WRITE(READ,INSERT.UPDATE,DELETE) PERMISSION*/ 
INSERT INTO PERMISSION(permission_id,permission_name) VALUES(2,'READ');  /* ONLY READ PERMISSION */ 
INSERT INTO PERMISSION(permission_id,permission_name) VALUES(3,'UPDATE'); /* ONLY UPDATE PERMISSION DOESNT INCLUDE CREATE,DELETE,READ*/
INSERT INTO PERMISSION(permission_id,permission_name) VALUES(4,'CREATE'); /* ONLY INSERT PERMISSION DOESNT INCLUDE UPDATE,DELETE,READ */
INSERT INTO PERMISSION(permission_id,permission_name) VALUES(5,'WRITE-ONLY'); /* ONLY WRITE(INSERT,UPDATE,DELETE) */

INSERT INTO ROLES(role_id,role_name,permission) VALUES(1,'ADMIN','{1}');
INSERT INTO ROLES(role_id,role_name,permission) VALUES(2,'TEACHER','{2,3}');
INSERT INTO ROLES(role_id,role_name,permission) VALUES(3,'NON-TEACHING STAFF','{2,3,4}');

INSERT INTO USERS(user_id,password,role_id) VALUES('vvinu9876@gmail.com','vinay vinu',1);


CREATE TABLE SESSION_HISTORY(
    SESSION_ID INT,
    USER_ID VARCHAR(50),
    TOKEN   VARCHAR(100),
    PRIMARY KEY(SESSION_ID),
    FOREIGN KEY(USER_ID) REFERENCES USERS(user_id)
);


CREATE TABLE TEACHER_LEAVES(
    leave_id integer GENERATED ALWAYS AS IDENTITY,
    teacher_id numeric(10),
    start_date DATE,
    end_date DATE,
    foreign key(teacher_id) references TEACHERS(tid)
);

CREATE TABLE  MISC(
    tid numeric(10),

    central_lv int,
    central_periods int,
    tech_lv int,
    tech_periods int,
    exam_lv int,
    exam_periods int,
    coi_lv int,
    coi_periods int,


    primary key(tid),
    FOREIGN KEY(tid) REFERENCES TEACHERS(TID)
);

ALTER TABLE SESSION_HISTORY ADD COLUMN IPADRESS varchar(50); 



CREATE OR REPLACE FUNCTION onSubjectAdd()
    RETURNS trigger AS    
$$
BEGIN
    UPDATE class  
    SET no_of_subjects = no_of_subjects + 1 ,
    NO_OF_HOURS_PER_WEEK = NO_OF_HOURS_PER_WEEK + (NEW.DURATION_OF_ONE_CLASS*NEW.NO_OF_CLASSES_PER_WEEK)
    WHERE class_id = new.class_id;
 
    UPDATE teachers 
    SET NO_OF_HOURS_PER_WEEK = NO_OF_HOURS_PER_WEEK + (NEW.DURATION_OF_ONE_CLASS*NEW.NO_OF_CLASSES_PER_WEEK)
    WHERE tid = new.teacher_id;

    RETURN NULL;
 
END;
$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION onSubjectDelete()
    RETURNS TRIGGER AS
$$
BEGIN
    UPDATE class  
    SET no_of_subjects = no_of_subjects - 1 ,
    NO_OF_HOURS_PER_WEEK = NO_OF_HOURS_PER_WEEK - (OLD.DURATION_OF_ONE_CLASS*OLD.NO_OF_CLASSES_PER_WEEK)
    WHERE class_id = OLD.class_id;
 
    UPDATE teachers 
    SET NO_OF_HOURS_PER_WEEK = NO_OF_HOURS_PER_WEEK - (OLD.DURATION_OF_ONE_CLASS*OLD.NO_OF_CLASSES_PER_WEEK)
    WHERE tid = OLD.teacher_id;

    RETURN OLD;
 
END;
$$
LANGUAGE plpgsql;


CREATE TRIGGER onsubadd
  AFTER INSERT
  ON SUBJECTS
  FOR EACH ROW
  EXECUTE PROCEDURE onSubjectAdd();

CREATE TRIGGER onsubdel
  BEFORE DELETE
  ON SUBJECTS
  FOR EACH ROW
  EXECUTE PROCEDURE onSubjectDelete();



