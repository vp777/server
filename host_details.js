'user strict';

const si = require('systeminformation');
var os = require('os');

console.log("===========================\n\nHost Details\n-----------------");
console.log("Host Platform :"+os.platform());
console.log("Host Name : "+os.hostname());

var ifaces = os.networkInterfaces();
Object.keys(ifaces).forEach(function (ifname) {
  var alias = 0;

  ifaces[ifname].forEach(function (iface) {
    if ('IPv4' !== iface.family || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }

    if (alias >= 1) {
      // this single interface has multiple ipv4 addresses
      console.log(ifname + ':' + alias, iface.address);
    } else {
      // this interface has only one ipv4 adress
      console.log(ifname, iface.address);
    }
    ++alias;
  });
});
si.cpu()
    .then(data =>{
      console.log("===========================\n\nCPU DETAILS\n-----------------");
      console.log("No of Processors : "+data.processors);
      console.log("CPU Manufaturer : "+data.manufacturer);
      console.log("CPI BRAND  :"+data.brand);
    })
    .catch(error => console.error(error));
const time_info = si.time();
console.log("===========================\n\nTIME INFORMATION\n-----------------");
console.log("TIMEZONE : "+time_info.timezone);
console.log("TIMEZONE NAME : "+time_info.timezoneName);
console.log("CURRENT TIME : "+time_info.current);

si.system()
    .then(data => {
      console.log("===========================\n\nSYSTEM INFORMATION\n-----------------");
      console.log("Manufacturer : "+data.manufacturer);
      console.log("Model : "+data.model);
      console.log("Version : "+data.version);
      console.log("Serial "+data.serial);
      console.log("UUID : "+data.uuid);
      console.log("SKU Number : "+data.sku);
    }).catch(error => console.error(error));

si.cpuTemperature().then(data =>{
  console.log("===========================\n\nCPU TEMPERATURE\n-----------------");
  console.log("Avg Temperate :"+data.main);
  data.cores.map(function(core,idx){
      console.log("Core "+idx+" :"+core);
  })
  console.log("Max Temperature :"+data.max);
})

module.exports = os;
