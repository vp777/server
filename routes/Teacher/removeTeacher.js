'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');

async function removeTeacher(db,id,res){
    const miscQuery = `DELETE FROM MISC WHERE TID=${id};`;
    const lecturesQuery = `DELETE FROM LECTURES WHERE teacher_id=${id};`;
    const subjectsQuery = `DELETE FROM SUBJECTS WHERE teacher_id=${id};`;
	return db.multi(`
                    ${miscQuery}
                    ${lecturesQuery}
                    ${subjectsQuery}
                    DELETE FROM TEACHERS WHERE TID=${id};
                    `, [])
            .then(function() {
                return true;
            })
            .catch(function(error) {
                // error;
                res.send({status:'failure',message:error.message});
            });

}


router.get('/',async function(req, res, next) {	
        var id = req.query.tid;
        var user_id = req.query.user_id;

        var depName = req.query.departmentName;
        var getConnection = require("../../dbConnections/getConnection");
        const db = getConnection(depName);
        
        var isLoggedIn = await checkLoggedIn(db,req,res);
        var hasPermission = await checkHasPermission(db,user_id,6,res); //check if the user has write or delete permission

        if(!isLoggedIn){
            res.send({status:'failure',message:'Unauthorized Access'});
        }

        if(!hasPermission){
            res.send({status:'failure',message:'Permission Denied'});
        }
        var removed = await removeTeacher(db,id,res);
        res.send({status:'success',message:"Teacher Removed Succesfully"});
});

module.exports = router;