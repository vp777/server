'use strict';
var express = require('express');
var router = express.Router();
const getTotalPDS = require('./getCompletedPDS');

router.get('/',async (req,res,next)=>{

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);

    const {tid} = req.query;

    if(!tid){
        console.log("Id cannot be empty");
        res.send({status:"failure",message:"Id cannot be empty"});
        return;
    }

    const query = "SELECT * FROM MISC WHERE TID = $1";

    const totalSubjects = "SELECT COUNT(*) as total_subjects FROM SUBJECTS WHERE TEACHER_ID=$1";
    const timeTableDate = "SELECT start_date FROM MISC_START_DATE";


    //console.log("query = ",query);

    const pdsValues = await getTotalPDS(db,tid);

    const totalPDS = pdsValues["totalPDS"];
    const totalPractical = pdsValues["totalPractical"];
    const totalTheory = pdsValues["totalTheory"];

    return db.any(query,[tid]).then((miscData)=>{
        //console.log("Data = ",data);
        return db.any(totalSubjects,[tid]).then((subjectCountData)=>{
            return db.any(timeTableDate,[]).then((timeTableData)=>{
                var toReturn = {
                    status:"success",
                    miscData:miscData[0],
                    subject_count:subjectCountData[0]["total_subjects"],
                    start_date:timeTableData.length>0?timeTableData[0]["start_date"]:null,
                    completedPDS: totalPDS,
                    totalPractical,
                    totalTheory
                };
               // console.log('returning ',toReturn);
                res.send(toReturn);
                return;
            }).catch((err)=>{throw err});
        }).catch((err)=>{throw err;})
    }).catch((err)=>{
        console.error(err);
        res.send({status:"failure",message:err.message});
    })

})

module.exports = router;