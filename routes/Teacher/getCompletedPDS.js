

const genDateQuery = "SELECT start_date from MISC_START_DATE";
const teacherDataQuery = `
select l.end_time,l.day_name,sub.subject_type,c.class_strength 
from lectures l,subjects sub,class c 
where l.teacher_id=$1
and
sub.subject_id = l.subject_id
and 
c.class_id = l.class_id
and 
(timetable_id=(select timetable_id from timetable order by timetable_id desc limit 1))`;


function getGeneratedDate(db){
    return db.any(genDateQuery).then((res)=>{
        if(res.length===0){
            return (new Date()).toDateString();
        }
        console.log("response = ",new Date(res[0]["start_date"]));
        return new Date(res[0]["start_date"]);
    }).catch((err)=>{
        console.error(err);
        throw err;
    })
}

async function getStatsForAllDays(db,tid){
    var days_stats = {
        "monday" : {total:0,theory:0,practical:0},
        "tuesday" : {total:0,theory:0,practical:0},
        "wednesday" : {total:0,theory:0,practical:0},
        "thursday" : {total:0,theory:0,practical:0},
        "friday" : {total:0,theory:0,practical:0},
        "saturday" : {total:0,theory:0,practical:0},
        "one_week" : 0,
        "one_week_theory" : 0 ,
        "one_week_practical" : 0 ,
    };

    const starttime = 0; // 5 am in the morning
    const endtime = 23; // 9 am in the night

    Object.keys(days_stats).forEach((day)=>{
        for(var i=starttime;i<=endtime;i++){
            days_stats[day][i] = 0;
            days_stats[day][i+"_theory"] = 0;
            days_stats[day][i+"_practical"] = 0;
        }
    });


    return db.any(teacherDataQuery,[tid]).then((result)=>{
        result.forEach((res)=>{
            const time = parseInt(res["end_time"].split(":")[0]);
            var valueToAdd = res["subject_type"]==="theory"?1:(1*(res["class_strength"]/5));
            //console.log("Value to add = ",valueToAdd);
            days_stats[res["day_name"]]["theory"] += (res["subject_type"]==="theory") ? 1 : 0;
            days_stats[res["day_name"]]["practical"] += (res["subject_type"]==="practical") ? valueToAdd : 0;
            days_stats[res["day_name"]][time] += valueToAdd;
            days_stats[res["day_name"]][time+"_theory"] += (res["subject_type"]==="theory"?1:0);
            days_stats[res["day_name"]][time+"_practical"] += (res["subject_type"]==="practical"?valueToAdd:0);
            days_stats[res["day_name"]]["total"] += valueToAdd;
            days_stats["one_week"] += valueToAdd;
            days_stats["one_week_theory"] += (res["subject_type"]==="theory"?1:0);
            days_stats["one_week_practical"] += (res["subject_type"]==="practical"?valueToAdd:0);
        })
        return days_stats;
    }).catch((err)=>{
        console.error(err);
        throw err;
    })
}

function getDiffDays(start,end){
    const diffInMs   = new Date(start) - new Date(end)
    const diffInDays = diffInMs / (1000 * 60 * 60 * 24);
    
    return diffInDays;
}

function getDayName(dateStr)
{
    var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
    var d = new Date(dateStr);
    var dayName = days[d.getDay()];     
    return dayName; 
}


async function getTotalPDS(db,tid){
    const startDate = await getGeneratedDate(db);
    //console.log("start date = ",startDate);
    const today = new Date();
    const diffDays = Math.abs(getDiffDays(startDate,today));

    const weeks = Math.round(diffDays/7);
    const remainingDays = diffDays - (weeks*7);

    const daysStats = await getStatsForAllDays(db,tid);

    var totalPDS = 0; 
    var totalTheory = 0;
    var totalPractical = 0;

    for(var i=0;i<weeks;i++){
        totalPDS += daysStats["one_week"];
        totalTheory += daysStats["one_week_theory"];
        totalPractical += daysStats["one_week_practical"];
    }    

    //console.log("total PDS = ",totalPDS);
    
    //console.log("weeks = ",weeks);
    //console.log("difference days = ",diffDays);
    //console.log("Remaining days = ",remainingDays);

    if(getDayName(today)==="sunday"){
        return totalPDS;
    }

    const currHours = today.getHours();

    const dayNo = today.getDay()-1;

    const limit = dayNo - 1; // 0 based counting , to get till yesterday

    const keys = Object.keys(daysStats);

    // till yesterday
    for(var i=0;i<limit;i++){
        totalPDS += daysStats[keys[i]]["total"];
        totalTheory += daysStats[keys[i]]["theory"];
        totalPractical += daysStats[keys[i]]["practical"];
    }

    //console.log('day no = ',dayNo);
    const todaysStats = daysStats[keys[dayNo]];
    //console.log("Todays stats = ",todaysStats);

    Object.keys(todaysStats).forEach((time)=>{
        //console.log("Key = " ,time);
        if(time==="total"){
            //console.log("Key is total");
        }
        else if(time==="theory"){
            //console.log("Key is theory");
        }
        else if(time==="practical"){
            //console.log("Key is practical");
        }
        else if(time.includes("practical")){
            let timeFormatted = time.split("_")[0];
            if(parseInt(timeFormatted)<=currHours){
                //console.log("Adding value to practical at time = ",timeFormatted);
                totalPractical += todaysStats[time];
            }
        }
        else if(time.includes("theory")){
            let timeFormatted = time.split("_")[0];
            if(parseInt(timeFormatted)<=currHours){
                //console.log("Adding value to theory at time = ",timeFormatted);
                totalTheory += todaysStats[time]
            }
        }
        else if(parseInt(time)<=currHours){
            totalPDS += todaysStats[time];
            //console.log(todaysStats[time]);
        } 
    })  

    //console.log("Total practical = ",totalPractical," Total theory = ",totalTheory," Total PDS = ",totalPDS);

    //console.log("PDs at the end = ",totalPDS);
    
    return {totalPDS,totalPractical,totalTheory};
}


module.exports = getTotalPDS;
