'use strict';
var express = require('express');
var router = express.Router();

router.get("/",async (req,res,next)=>{
    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);

    const {teacher_id} = req.query;

    if(!teacher_id){
        res.send({status:"failure",message:"Teacher id cannot be empty"});
        return;
    }

    const query = "select * from subjects where teacher_id=$1";

    return db.any(query,[teacher_id]).then((data)=>{
        res.send({status:"success",data});
        return;
    }).catch((err)=>{
        console.error(err);
        res.send({status:"failure",message:err.message});
        return;
    })

})  

module.exports = router;