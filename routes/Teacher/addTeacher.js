'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');

async function getID(db,res){
    
    return db.any('SELECT TID FROM TEACHERS ORDER BY TID DESC LIMIT 1', [true])
    .then(function(data) {
        // success;
        return data.length>0?(parseInt(data[0].tid)+1):1;
    })
    .catch(function(error) {
        // error;
        console.log(error);
        res.send({status:'error',message:'error.message'});
    });
}


router.get('/',async function(req, res, next) {
    var name   =    req.query.name;
    var phone  =    req.query.phone;
    var email  =    req.query.email;
    var authorized_lectures = req.query.authorized_lectures;
    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);
    var new_id =    await getID(db,res);

    console.log("Authorized Lectures = ",authorized_lectures);

    try{
        authorized_lectures = parseInt(authorized_lectures);
        console.log("Authorized lectures = ",authorized_lectures);
    }
    catch(e){
        console.error(e); 
        res.send({status:"failure",message:e.message});
        return;
    }

    var user_id = req.query.user_id;
        
    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,4,res); //check if the user has write or create permission

    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
        return;
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
        return;
    }

    console.log(new_id);

    const createMiscQuery = `INSERT INTO MISC (tid,central_lv,central_periods,tech_lv,tech_periods,exam_lv,exam_periods,coi_lv,coi_periods) values (${new_id},0,0,0,0,0,0,0,0)`;
    
    return db.none('INSERT INTO TEACHERS(TID,NAME,PHONE,EMAIL,NO_OF_HOURS_PER_WEEK,AUTHORIZED_LECTURES) VALUES($1, $2, $3, $4, $5,$6)', [new_id,name,phone,email,0,authorized_lectures])
    .then(() => {
        return db.none(createMiscQuery).then(()=>{
            res.send({status:'success',message:'Teacher Added Succesfully'});
            return;
        })
    })
    .catch(error => {
        console.log('ERROR:', error.message); // print error;
        res.send({status:'error',message:error.message});
    })
            
});
  
module.exports = router;