'use strict';
var express = require('express');
var router = express.Router();
var db = require('../../db');

router.get('/',async (req,res,next)=>{

    const {tid,central_lv,central_periods,tech_lv,tech_periods,exam_lv,exam_periods,coi_lv,coi_periods} = req.query;

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);

    if(!tid){
        console.log("Id cannot be empty");
        res.send({status:"failure",message:"Id cannot be empty"});
        return;
    }

    if(((!central_lv)  && (central_lv!==0)) ||  (!central_periods && (central_periods!==0)) || 
        (!tech_lv && (tech_lv!==0)) || (!tech_periods && (tech_periods!==0)) || 
        (!exam_lv && (exam_lv!==0)) || (!exam_periods && (exam_periods!==0)) || 
        (!coi_lv && (coi_lv!==0)) || (!coi_periods && (coi_periods!==0))){
        console.log("Values cannot be null or NAN");
        res.send({status:"failure",message:"Values must be a number"});
        return;
    }

    const query = `UPDATE MISC SET central_lv=${central_lv},central_periods=${central_periods},tech_lv=${tech_lv},tech_periods=${tech_periods},exam_lv=${exam_lv},exam_periods=${exam_periods},coi_lv=${coi_lv},coi_periods=${coi_periods} where tid=${tid}`;

    console.log("query = ",query);

    return db.any(query,[]).then((data)=>{
        //console.log("Data = ",data);
        res.send({"status":'success',data:data[0]});
        return;
    }).catch((err)=>{
        console.error(err);
        res.send({status:"failure",message:err.message});
    })

})

module.exports = router;