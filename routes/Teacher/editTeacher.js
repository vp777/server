'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');



router.get('/',async function(req, res, next) {
    var name   =    req.query.name;
    var phone  =    req.query.phone;
    var email  =    req.query.email;
    var id     =    req.query.id;
    var user_id = req.query.user_id;

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);
        
    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,3,res); //check if the user has write or delete permission

    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
        return;
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
        return;
    }
    return db.none('UPDATE  TEACHERS SET NAME=$1,EMAIL=$2,PHONE=$3 where TID=$4', [name,email,phone,id])
    .then(data => {
        res.send({status:'success',message:'Teacher Updated Succesfully'});
    })
    .catch(error => {
        console.log('ERROR:', error.message); // print error;
        res.send({status:'error',message:error.message});
    })
            
});
  
module.exports = router;