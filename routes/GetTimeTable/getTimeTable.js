'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');

router.get('/',async function(req, res, next) {
    var user_id = req.query.user_id;

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);
        
    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,2,res); //check if the user has write or read permission

    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
    }
    var class_id = req.query.class_id;
    var day = req.query.day;
    
    var query =  `
    select sub.subject_name,
	  r.room_name,
	  r.room_no,
	  teach.name,
	  l.start_time,
	  l.end_time 
	  from
	  subjects sub,
	  room r,
	  teachers teach,
	  lectures l
	  where 
	  sub.subject_id = l.subject_id and
	  r.room_no = l.room_id and
	  teach.tid = l.teacher_id and
	  l.class_id = $1 and l.day_name=$2 and 
	  l.timetable_id = (select timetable_id from timetable order by timetable_id desc limit 1)
      order by l.start_time;
    `
    return db.any(query ,[parseInt(class_id),day] ).then(data =>{
        console.log("Sending Data");
        res.send({status:'success',data:data});
    }).catch(function(error){
        console.log('ERROR:', error.message); // print error;
        res.send({status:'error',message:error.message});
    })

});


module.exports = router;