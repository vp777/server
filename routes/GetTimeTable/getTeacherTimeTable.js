'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');


router.get('/',async function(req, res, next) {
    var teacher_id = req.query.teacher_id;
    var day = req.query.day;
    var user_id = req.query.user_id;
    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);
        
    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,2,res); //check if the user has write or read permission

    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
        return;
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
        return;
    }
    
    var query = `SELECT SUB.SUBJECT_NAME as subject_name,
                        SUB.ROOM_ID as room_id,
                        SUB.SUBJECT_ID as subject_id,
                        SUB.DURATION_OF_ONE_CLASS as duration,
                        to_char(LECT.start_time,'HH12:MI:SS') as start_time,
                        to_char(LECT.end_time,'HH12:MI:SS') as end_time,
                        CLS.CLASS_NAME as class_name
                        FROM 
                        LECTURES LECT
                        INNER JOIN 
                        SUBJECTS SUB
                        ON SUB.SUBJECT_ID = LECT.SUBJECT_ID
                        INNER JOIN
                        CLASS CLS
                        ON CLS.CLASS_ID = LECT.CLASS_ID
                        WHERE LECT.TEACHER_ID = $1 
                        AND
                        LECT.DAY_NAME = $2 
                        AND 
                        LECT.TIMETABLE_ID = (
                            SELECT TIMETABLE_ID FROM TIMETABLE ORDER BY TIMETABLE_ID  DESC LIMIT 1 
                        )
                        ORDER BY LECT.START_TIME ASC 
                `;

    db.any(query,[teacher_id,day])
    .then(data=>{
        console.log("Sending Data...");
        console.log("Data = ",data);
        res.send({status:'success',data:data});
    })
    .catch(function(error){
        console.error(error);
        res.send({status:'failure',message:error.message});
    })
})


module.exports = router;