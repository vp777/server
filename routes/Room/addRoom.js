'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');


router.get('/',async function(req, res, next) {
    var name   =    req.query.name;
    var no     =    req.query.no;
    var user_id = req.query.user_id;
    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);
        
    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,4,res); //check if the user has write or create permission

    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
        return;
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
        return;
    }

    if(name===null || name===undefined || no===null || no===undefined){
        res.send({status:'error',message:'Kindly Enter All Details'});
    }

    return db.none('INSERT INTO ROOM(ROOM_NO,ROOM_NAME) VALUES($1, $2)', [no,name])
    .then(data => {
        console.log("New Room Added"); // print new user id;
        res.send({status:'success',message:'Room Added Succesfully'});
    })
    .catch(error => {
        console.log('ERROR:', error.message); // print error;
        res.send({status:'error',message:error.message});
    })
            
});
  
module.exports = router;