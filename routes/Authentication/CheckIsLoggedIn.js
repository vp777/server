var ipaddr = require('ipaddr.js');

function getFormattedIP(ipString){
    if (ipaddr.IPv4.isValid(ipString)) {
            // ipString is IPv4
            return ipaddr;
    } 
    else if (ipaddr.IPv6.isValid(ipString)) {
            var ip = ipaddr.IPv6.parse(ipString);
            if (ip.isIPv4MappedAddress()) {
                    // ip.toIPv4Address().toString() is IPv4
                    return ip.toIPv4Address().toString();
            } 
            else {
                    // ipString is IPv6
                    return ipString;
            }
    } 
    else {
        // ipString is invalid
        return "Invalid";
    }
}


async function isLoggedIn(db,req, res) {
    var user_id = req.query.user_id;
    var token   = req.query.token;
    console.log(user_id,token);
    var ipAddress = getFormattedIP(req.connection.remoteAddress);

    if(ipAddress==='invalid'){
        res.send({status:'failure',message:'Invalid IP Address'});
    }
    
    return db.any('SELECT count(*) as count from session_history where user_id = $1 and token = $2 and ipadress = $3',[user_id,token,ipAddress])
          .then(data=>{
              if(data.length>0){
                  return true;
              }
              return false;
          }).catch(function(error){
              console.error(error);
              res.send({status:'failure',message:error.message});
          })
};

module.exports = isLoggedIn;