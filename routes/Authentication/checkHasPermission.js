
function hasPermission(perm_arr,id){
    for(var i=0;i<perm_arr.length;i++){
        if(String(perm_arr[i])===String(id)){
            return true;
        }
    }
    return false;
}

async function checkPermission(db,user_id,permission_id,res){
    return db.one('SELECT permission from roles r, users u where r.role_id = u.role_id and u.user_id = $1',[user_id])
        .then(data=>{
            var permissions = data.permission;
            if(hasPermission(permissions,1)){
                return true;
            }
            else if(hasPermission(permissions,permission_id)){
                return true;
            }
            else{
                return false;
            }

        })
        .catch(function(err){
            console.error(err);
            res.send({status:'failure',message:err.message});
        })
}


module.exports = checkPermission;