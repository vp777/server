'use strict';

/*

 AUTH SYSTEM GUIDELINES
------------------------
1.Client sends credentials to the server
2.Server verifies the credentials, generates a JWT and sends it back as a response
3.Subsequent requests from the client have a JWT in the request headers
4.Server validates the token and if valid, provide the requested response.

*/

var express = require('express');
var router = express.Router();
var sha256 = require('sha256');
var ipaddr = require('ipaddr.js');
const getConnection = require("../../dbConnections/getConnection");


async function checkUser(db,username,password,res){
    //var hashpassword = sha256.x2(password);
    return db.any('SELECT user_id FROM USERS WHERE USER_ID = $1 AND PASSWORD= $2', [username,password])
             .then(function(data) {
                    // success;
                    if(data.length>0)
                        return true;
                    else    
                        res.send({status:'failure',message:'Invalid Credentials'});   
            })
            .catch(function(error) {
                // error;
                console.log(error);
                res.send({status:'failure',message:error.message});
            });
}
async function getSessionId(db,res){
    return db.any('SELECT SESSION_ID FROM SESSION_HISTORY ORDER BY SESSION_ID DESC LIMIT 1', [true])
    .then(function(data) {
        // success;
        return data.length>0?(parseInt(data[0].session_id)+1):1;
    })
    .catch(function(error) {
        // error;
        console.log(error);
        res.send({status:'error',message:'error.message'});
    });
}

function getFormattedIP(ipString){
    if (ipaddr.IPv4.isValid(ipString)) {
            // ipString is IPv4
            return ipaddr;
    } 
    else if (ipaddr.IPv6.isValid(ipString)) {
            var ip = ipaddr.IPv6.parse(ipString);
            if (ip.isIPv4MappedAddress()) {
                    // ip.toIPv4Address().toString() is IPv4
                    return ip.toIPv4Address().toString();
            } 
            else {
                    // ipString is IPv6
                    return ipString;
            }
    } 
    else {
        // ipString is invalid
        return "Invalid";
    }
}

async function addSession(db,username,token,ipaddress,res){
    var session_id = await getSessionId(db,res);
    return db.none('INSERT INTO session_history(session_id,user_id,token,ipadress) VALUES($1, $2, $3,$4)', [session_id, username,token,ipaddress])
            .then(() => {
                    // success;
                    return true;
            })
            .catch(error => {
                    // error;
                    res.send({status:'failure',message:error.message});
            });
}

function getToken(){
    var result = '';
    var length  = 100;
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

router.get('/',async function(req, res, next) {
        var username = req.query.username;
        var password = req.query.password;
        const depName = req.query.departmentName;
        var ipAddress = getFormattedIP(req.connection.remoteAddress);
        if(ipAddress==='invalid'){
            res.send({status:'failure',message:'Invalid IP Address'});
            return;
        }
        const db = getConnection(depName);
        console.log("Got db = ",db);
        console.log(username,password,ipAddress);
        if(await checkUser(db,username,password,res)){
            var token = getToken();
            await addSession(db,username,token,ipAddress,res);
            res.send({status:'success',message:token}); //send token in message
        }
        else{
            res.send({status:'failure',message:'Unkown error occured'});
        }
});

module.exports = router
