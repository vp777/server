'use strict';
var express = require('express');
var router = express.Router();

router.get('/',async function(req, res, next) {

    var start_date = req.query.start_date;

    if(!start_date){
        res.send({status:"failure",message:"Start date is not valid"});
        return;
    }

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);

    const query = `UPDATE MISC_START_DATE SET start_date = '${start_date}' where start_date=(SELECT start_date from MISC_START_DATE limit 1)`;

    return db.none(query).then(()=>{
        res.send({status:"success"});
        return;
    }).catch((err)=>{
        console.error(err);
        res.send({status:"failure",message:err.message});
        return;
    })

})

module.exports = router;