'use strict';
var express = require('express');
var router = express.Router();

router.get('/',async function(req, res, next) {

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);

    const query = `SELECT start_date from MISC_START_DATE`;

    return db.any(query).then((data)=>{
        console.log("Start date = ",data[0]["start_date"]);
        res.send({status:"success",start_date:data[0]["start_date"]});
        return;
    }).catch((err)=>{
        console.error(err);
        res.send({status:"failure",message:err.message});
        return;
    })

})

module.exports = router;