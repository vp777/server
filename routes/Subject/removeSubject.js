'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');

async function removeSubject(db,id,res){
    const deleteLectures = `DELETE FROM LECTURES WHERE SUBJECT_ID='${id}';`;
	return db.multi(`
            ${deleteLectures}
            DELETE FROM SUBJECTS WHERE SUBJECT_ID='${id}'`)
            .then(function() {
                // success;
                return true;
            })
            .catch(function(error) {
                // error;
                res.send({status:'failure',message:error.message});
            });

}


router.get('/',async function(req, res, next) {	
        var id = req.query.id;
        var user_id = req.query.user_id;

        var depName = req.query.departmentName;
        var getConnection = require("../../dbConnections/getConnection");
        const db = getConnection(depName);
        
        var isLoggedIn = await checkLoggedIn(db,req,res);
        var hasPermission = await checkHasPermission(db,user_id,6,res); //check if the user has write or delete permission

        if(!isLoggedIn){
            res.send({status:'failure',message:'Unauthorized Access'});
            return;
        }

        if(!hasPermission){
            res.send({status:'failure',message:'Permission Denied'});
            return;
        }
        
        var removed = await removeSubject(db,id,res);
        res.send({status:'success',message:"Subject Removed Succesfully"});
});

module.exports = router;