'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');


router.get('/',async function(req, res, next) {
    var user_id = req.query.user_id;

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);
        
    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,4,res); //check if the user has write or delete permission

    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
        return;
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
        return;
    }

    var id   =    req.query.id;
    var name  =    req.query.name;
    var no_of_classes  =    req.query.no_of_classes;
    var duration    = req.query.duration;
    var class_id    = req.query.class_id;
    var teacher_id    = req.query.teacher_id;
    var room_id    = req.query.room_id;
    var subject_type = req.query.subject_type;

    if(subject_type!=="theory" && subject_type!=="practical"){
        console.log("Subject type = ",subject_type);
        res.send({status:"failure",message:"Invalid subject type"});
        return;
    }
    
    return db.none('INSERT INTO SUBJECTS(SUBJECT_ID,SUBJECT_NAME,CLASS_ID,TEACHER_ID,ROOM_ID,DURATION_OF_ONE_CLASS,NO_OF_CLASSES_PER_WEEK,SUBJECT_TYPE) VALUES($1, $2, $3, $4, $5, $6, $7,$8)', [id,name,class_id,teacher_id,room_id,duration,no_of_classes,subject_type])
    .then(data => {
        res.send({status:'success',message:'Subject Added Succesfully'});
        return;
    })
    .catch(error => {
        console.log('ERROR:', error.message); // print error;
        res.send({status:'error',message:error.message});
        return;
    })
            
});
  
module.exports = router;