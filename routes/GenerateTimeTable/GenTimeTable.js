'use strict';
var express = require('express');
var router = express.Router();
const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');

/*==================================================


-----------------------------------------------
Time Table Generation using Heuristic Approach
-----------------------------------------------

Input Data : 
    1. TEACHERS  : DATA DESCRIBES THE TEACHER (ID) NOTE : Here we have taken only id as we can identify every teacher with unique id
    2. SUBJECTS  : DATA DESCRIBES THE COURSE (SUBJECT_ID,SUBJECT_NAME,CLASS_ID,TEACHER_ID,ROOM_ID,DURATION,NO_OF_CLASSES_PER_WEEK)
    3. CLASS     : DATA DESCRIBES THE CLASS (ID)
    4. ROOM      : DATA DESCRIBES THE ROOM  (ID) 
    5. DAYS      : DATA DESCRIBES TIMINGS IN PARTICAULAR DAY (START_TIME , END_TIME)
    6. BREAKS    : DATA DESCTIBES BREAKS (NO_OF_BREAKS , [{START_TIME,END_TIME}.....N])



HARD CONSTRAINTS (MUST BE SATISFIED) :
-------------------------------------
    
1. A CLASS SHOULD HAVE ONLY ONE LECTURE AT A TIME
2. A TEACHER SHOULD HAVE ONLY ONE CLASS  AT A TIME
3. A ROOM SHOULD HAVE ONLY BE OCCUPIED BY ONLY ONE CLASS AT A TIME
4. NO CLASSES DURING BREAK TIMES


SOFT CONSTRAINTS (NOT NECCESSARY TO BE SATISFIED):
--------------------------------------------------

1. CLASS SHOULD NOT HAVE FREE TIME BETWEEN TWO LECTURES IN A 
2. TEACHER SCHEDULE SHOULD BE WELL SPREAD OVER THE WEEK


==================================================*/



var days = ['monday','tuesday','wednesday','thursday','friday','saturday'];

//use 24-hour format 
//INPUT : 
//TIMESEGMENTS = [{START_TIME:"HH:MM",END_TIME:'HH:MM}] 
//START_TIME = "HH:MM" 
//END_TIME : "HH:MM" 
//OUTPUT : BOOLEAN 
let overlap = (timeSegments,start_time,end_time) => { //give time strings like "11:30 12:30"
    let ret = false;
    let i = 0;
    while( !ret && i<timeSegments.length ){
      let seg = timeSegments[i];
      let range1 = moment.range( moment(start_time, 'HH:mm'),  moment(end_time, 'HH:mm'));
      let range2 = moment.range( moment(seg.start_time, 'HH:mm'),  moment(seg.end_time, 'HH:mm'));
      if( range1.overlaps(range2) ){
        ret = true;
      }
      i++;
    }
    return ret;
  };


function addMinutes(time, minsToAdd) { //input 24hr format
   return moment(time,"HH:mm").add(minsToAdd,'minutes').format('HH:mm');//returns 24 hr format string
}


async function getData(db,table_name,orderby,res){
    return db.any('SELECT * FROM '+table_name+' ORDER BY '+orderby+' ASC', [])
            .then(function(data) {
                // success;
                return data;
            })
            .catch(function(error) {
                // error;
                console.error(error);
                res.send({status:'failure',message:error.message});
            });
}


async function getNewTimeTableID(db,res){
    return db.any('SELECT timetable_id FROM TIMETABLE ORDER BY timetable_id desc limit 1', [])
            .then(function(data) {
                // success;
                var new_id = data.length>0?(parseInt(data[0].timetable_id)+1):1;
                var date = new Date();
                var time = moment(date).format('HH:mm:ss');
                date = moment(date).format('YYYY/MM/DD');
                return db.none('INSERT INTO TIMETABLE(TIMETABLE_ID,GEN_DATE,GEN_TIME) VALUES ($1,$2,$3)',[new_id,date,time]).then(function(){
                    return new_id;
                })
                .catch(function(error) {
                    // error;
                    console.error(error);
                    res.send({status:'failure',message:error.message});
                });
               
            })
            .catch(function(error) {
                // error;
                console.error(error);
                res.send({status:'failure',message:error.message});
            });
}



async function addLecture(db,res,start_time,end_time,timetable_id,day,lecture,class_id){
    console.log(`Adding lecture res = ${res} start_time=${start_time} end_time=${end_time} timetable_id=${timetable_id} day=${day} lecture=${lecture} class_id=${class_id}`);
    console.log(lecture);
    //id is generated automatically so dont care :)
    db.none('INSERT INTO LECTURES  (start_time,end_time,subject_id,timetable_id,day_name,teacher_id,room_id,class_id) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)',
            [start_time,end_time,lecture.subject_id,timetable_id,day,lecture.teacher_id,lecture.room_id,class_id])
    .catch(function(err){
        res.send({status:'failure',message:err.message});
    });
}
     


function addBreakTime(lectures,start_time,end_time){
    days.forEach(function(day){
        var day_lecture = lectures[day];
        for(var class_id in day_lecture){
            if(day_lecture.hasOwnProperty(class_id)){
                lectures[day][class_id].push({start_time,end_time,tid:null});
            }
        }
    })
    return lectures;
}



function inRange(srange,erange,curr){
    var format = 'HH:mm'
    var time = moment(curr,format);
    var start = moment(srange,format).subtract(1,"minutes"); //to clear equal issues
    var end  = moment(erange,format).add(1,"minutes");
    return time.isBetween(start,end);
}

function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
}

function removeAtIndex(array,index){
    if (index > -1) {
        array.splice(index, 1);
    }
    return array;
}


function decreaseWeekClasses(subject_id,subjects_data){
    for(var i=0;i<subjects_data.length;i++){
        if(subjects_data[i].subject_id==subject_id){
            subjects_data[i].no_of_classes_per_week = subjects_data[i].no_of_classes_per_week - 1;
            return subjects_data;
        }
    }
    return subjects_data;
}



function uniq_fast(a) {
    var seen = {};
    var out = [];
    var len = a.length;
    var j = 0;
    for(var i = 0; i < len; i++) {
         var item = a[i];
         if(seen[item] !== 1) {
               seen[item] = 1;
               out[j++] = item;
         }
    }
    return out;
}

router.get('/',async function(req, res, next) {

        var user_id = req.query.user_id;

        var depName = req.query.departmentName;
        var getConnection = require("../../dbConnections/getConnection");
        const db = getConnection(depName);

        var isLoggedIn = await checkLoggedIn(db,req,res);
        var hasPermission = await checkHasPermission(db,user_id,7,res); //check if the user has write or generate permission

        if(!isLoggedIn){
            res.send({status:'failure',message:'Unauthorized Access'});
        }

        if(!hasPermission){
            res.send({status:'failure',message:'Permission Denied'});
        }

        var config_data = JSON.parse(req.query.data);
        //var config_data = {break_timings:[{start_time:'11:30',end_time:'12:30'},{start_time:'1:30',end_time:'2:30'}]};


        var subject_data = [];
        var rooms        = [];
        var teachers     = [];
        var classes      = [];
        var lectures     = {};

        var class_subjects = {};

        await getData(db,'subjects','subject_id',res).then(function(result){
                subject_data = result;
        }).catch(function(err){
            console.log(err);
            res.send({status:'failure',message:error.message});
        })

        var lectures = {
            monday : {}, //{class_id:[]}
            tuesday : {},
            wednesday : {},
            thursday : {},
            friday : {},
            saturday : {},
        };

        var teachers_schedule = {};

        var room_schedule = {};

        var samp_schedule = {
            monday : [],
            tuesday : [],
            wednesday : [],
            thursday : [],
            friday : [],
            saturday : [],
        }

        subject_data.forEach(function(subject){
            rooms.push(subject.room_id);
            teachers.push(subject.teacher_id);
            classes.push(subject.class_id);

            for(var day in lectures){
                lectures[day][subject.class_id] = [];
            }
        })

        for(var t in teachers){
            teachers_schedule[teachers[t]] = samp_schedule;
        }

        for(var r in rooms){
            room_schedule[rooms[r]] = samp_schedule;
        }

        for(var k in classes){
            class_subjects[classes[k]] = [];
        }

        for(var k in subject_data){
            subject_data[k].no_of_classes_per_week = parseInt(subject_data[k].no_of_classes_per_week);
            subject_data[k].duration_of_one_class = parseFloat(subject_data[k].duration_of_one_class);
            class_subjects[subject_data[k].class_id].push(subject_data[k]);
        }


        config_data.break_timings.forEach(function(break_time){
            lectures = addBreakTime(lectures,break_time.start_time,break_time.end_time);
        })

        console.log("Processing duplicated");
        classes = uniq_fast(classes);
        console.log(classes);
        
        days.forEach(function(day){
            
            console.log("processing "+day);
            const day_start_time = config_data.day_timings[day].start_time;
            const day_end_time   = config_data.day_timings[day].end_time;
            
            console.log("Start=",day_start_time,"End = ",day_end_time);
            classes.forEach(function(class_id){

                console.log("Processing class ",class_id);
                var curr_time  = day_start_time;
                
                console.log("Curr Time = ",curr_time," in range = ", inRange(day_start_time,day_end_time,curr_time));
                var currClassSubjects = [];
                    //fliter
                    class_subjects[class_id].forEach(function(subject_det){
                        if(subject_det.no_of_classes_per_week!==0){
                            currClassSubjects.push(subject_det);
                        }
                    })
                while(inRange(day_start_time,day_end_time,curr_time) && (currClassSubjects.length>0)){
                    console.log("Curr time = ",curr_time);
                    shuffle(currClassSubjects);
                    var prevlength = currClassSubjects.length;
                    currClassSubjects.forEach(function(subject_det,index){
                            var sub_start_time = curr_time;
                            var sub_end_time = addMinutes(curr_time,60 * subject_det.duration_of_one_class);
                            
                            if((!overlap(teachers_schedule[subject_det.teacher_id][day],sub_start_time,sub_end_time)) &&
                                (!overlap(room_schedule[subject_det.room_id][day],sub_start_time,sub_end_time) &&
                                (!overlap(lectures[day][class_id],sub_start_time,sub_end_time))) &&
                                (inRange(day_start_time,day_end_time,sub_start_time) && inRange(day_start_time,day_end_time,sub_end_time))
                               ){
                                   
                                const sched_det = {start_time:sub_start_time,end_time:sub_end_time};
                                room_schedule[subject_det.room_id][day].push(sched_det);
                                teachers_schedule[subject_det.teacher_id][day].push(sched_det);
                                lectures[day][class_id].push(
                                                        {
                                                            start_time:sub_start_time,
                                                            end_time : sub_end_time,
                                                            subject_id : subject_det.subject_id, 
                                                            teacher_id : subject_det.teacher_id,
                                                            room_id : subject_det.room_id,   
                                                        });

                                class_subjects[class_id] = decreaseWeekClasses(subject_det.subject_id,class_subjects[class_id]) 
                                curr_time = sub_end_time;
                                currClassSubjects = removeAtIndex(currClassSubjects,index); 
                                return false; //to break for each loop because we updated the array 
                            }
                        })
                        if(prevlength===currClassSubjects.length){
                            curr_time = addMinutes(curr_time,10);
                        }
                        if(!inRange(day_start_time,day_end_time,curr_time)){
                            break;
                        }
                        
                    }         
            })
        })


        /*================================================

         ADD Time Table to Database

        ==================================================*/
        
        console.log("Lectures = ",lectures);
        
        await getNewTimeTableID(db,res).then(function(timetable_id){
            days.forEach(function(day){
                classes.forEach(function(class_id){
                    var temp = lectures[day][class_id];
                    temp.forEach(function(lecture){
                            var start_time = moment(lecture.start_time,'HH:mm:ss').format('HH:mm:ss');
                            var end_time = moment(lecture.end_time,'HH:mm:ss').format('HH:mm:ss');
                            addLecture(db,res,start_time,end_time,timetable_id,day,lecture,class_id);
                    })
                })
            })

        }).catch(function(err){console.log(err);res.send(err)});




        res.status(200).send({status:"success",data:lectures});
})

  
module.exports = router;