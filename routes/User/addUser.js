'use strict';
var express = require('express');
var router = express.Router();
var sha256 = require('sha256');
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');


router.get('/',async function(req, res, next) {
    console.log("Here at add user");
    var user_name   =    req.query.uname;
    var add_user_id     =    req.query.add_user_id;
    var password    =    sha256.x2(req.query.password);
    var role_id     =    req.query.role_id;
    var user_id = req.query.user_id;

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);

    console.log("USer id = ",add_user_id," User name = ",user_name," Password = ",password," Role id =",role_id);
        
    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,4,res); //check if the user has write or delete permission

    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
        return;
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
        return;
    }

    console.log(user_name);
    console.log("USer id = ",add_user_id," User name = ",user_name," Password = ",password," Role id =",role_id);
    return db.none('INSERT INTO USERS(USER_ID,USER_NAME,PASSWORD,ROLE_ID) VALUES($1, $2, $3, $4)', [add_user_id,user_name,password,role_id])
    .then(data => {
        res.send({status:'success',message:'User Added Succesfully'});
    })
    .catch(error => {
        console.log('ERROR:', error.message); // print error;
        res.send({status:'error',message:error.message});
    })
            
});
  
module.exports = router;
