'use strict';
var express = require('express');
var router = express.Router();
var db = require('../../db');
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');



router.get('/',async function(req, res, next) {
    var user_id = req.query.user_id;

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);
        
    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,2,res); //check if the user has write or delete permission

    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
        return;
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
        return;
    }

    await db.any('SELECT USERS.*,ROLES.ROLE_NAME FROM USERS,ROLES WHERE ROLES.ROLE_ID = USERS.ROLE_ID order by USERS.USER_NAME',[])
        .then(data=>{
        res.send({status:'success',data:data});
    }).catch(function(err){
        res.send({status:'failure',message:err.message});
    })

});

module.exports = router;