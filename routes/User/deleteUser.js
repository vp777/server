'use strict';
var express = require('express');
var router = express.Router();
var db = require('../../db');
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');


router.get('/',async function(req, res, next) {
    var user_id_to_del = req.query.user_id_to_del;
    var user_id = req.query.user_id;

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);
        
    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,6,res); //check if the user has write or delete permission

    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
        return;
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
        return;
    }

   await db.none('DELETE FROM USERS WHERE USER_ID = $1',[user_id_to_del])
    .then(data=>{
        res.send({status:'success',message:'Succesfully Deleted'});
    }).catch(error=>{
        res.send({status:'failure',message:'Delete Unsuccessful'})
    })
 
});
  
module.exports = router;