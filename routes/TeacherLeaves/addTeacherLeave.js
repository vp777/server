'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');


router.get('/',async function(req, res, next) {
    var teacher_id   =    req.query.teacher_id;
    var start_date  =    req.query.start_date;
    var end_date  =    req.query.end_date;

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);

    var user_id = req.query.user_id;
        
    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,4,res); //check if the user has write or create permission

    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
        return;
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
        return;
    }

    console.log("Teacher id = ",teacher_id," Start_date = ",start_date," End date = ",end_date);

    const insertQuery = `INSERT INTO TEACHER_LEAVES(TEACHER_ID,START_DATE,END_DATE) VALUES ($1,$2,$3)`;

    return db.none(insertQuery,[teacher_id,start_date,end_date]).then(()=>{
        console.log("Teacher leave inserted succesfully");
        res.send({status:"success"});
        return;
    }).catch((err)=>{
        console.error(err);
        res.send({status:"failure",message:err.message});
        return;
    })
})

module.exports = router;