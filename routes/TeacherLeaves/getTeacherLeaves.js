'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');


router.get('/',async function(req, res, next) {
    var teacher_id   =    req.query.teacher_id;

    var depName = req.query.departmentName;
    var getConnection = require("../../dbConnections/getConnection");
    const db = getConnection(depName);

    var user_id = req.query.user_id;
        
    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,4,res); //check if the user has write or create permission

    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
        return;
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
        return;
    }

    const selectQuery = `SELECT * FROM TEACHER_LEAVES WHERE TEACHER_ID=$1`;

    return db.any(selectQuery,[teacher_id]).then((result)=>{
        console.log("Teacher leave inserted succesfully");
        res.send({status:"success",data:result});
        return;
    }).catch((err)=>{
        console.error(err);
        res.send({status:"failure",message:err.message});
        return;
    })
})

module.exports = router;