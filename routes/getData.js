'use strict';
var express = require('express');
var router = express.Router();
var getConnection = require("../dbConnections/getConnection");

async function getData(db,name,orderby,res){

    return db.any('SELECT * FROM '+name+' ORDER BY '+orderby+' ASC', [])
            .then(function(data) {
                // success;
                return data;
            })
            .catch(function(error) {
                // error;
                console.error(error);
                res.send({status:'failure',message:error.message});
            });
}

router.get('/',async function(req, res, next) {
        var name       = req.query.name;
        var orderby    = req.query.orderby;
        var departmentName = req.query.departmentName;
        const db = getConnection(departmentName);
        console.log(name,orderby);

        var data  = await getData(db,name,orderby,res).catch(function(err){
                                    console.log(err);
                                    res.send({status:'failure',message:err.message});
                                }).catch((err)=>{console.error(err)});
        res.send({status:'success',data:data});
});

module.exports = router;