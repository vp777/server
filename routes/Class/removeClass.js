'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');

async function removeClass(class_id,res){
	return db.none('DELETE FROM CLASS WHERE CLASS_ID=$1', [class_id])
            .then(function(data) {
                // success;
                return true;
            })
            .catch(function(error) {
                // error;
                res.send({status:'failure',message:error.message});
            });

}


router.get('/',async function(req, res, next) {	
        var class_id = req.query.class_id;
        var user_id = req.query.user_id;

        var depName = req.query.departmentName;
        var getConnection = require("../../dbConnections/getConnection");
        const db = getConnection(depName);
        
        var isLoggedIn = await checkLoggedIn(db,req,res);
        var hasPermission = await checkHasPermission(db,user_id,6,res); //check if the user has write or delete permission

        if(!isLoggedIn){
            res.send({status:'failure',message:'Unauthorized Access'});
            return;
        }

        if(!hasPermission){
            res.send({status:'failure',message:'Permission Denied'});
            return;
        }

        var removed = await removeClass(db,class_id,res);
        
        res.send({status:'success',message:"Removed class Succesfully"});
});

module.exports = router;