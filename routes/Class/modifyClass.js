'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');

async function modifyClass(db,name,id,res){
	return db.none('UPDATE CLASS SET CLASS_NAME=$1 WHERE CLASS_ID=$2', [name,id])
            .then(function(data) {
                // success;
                return true;
            })
            .catch(function(error) {
                // error;
                res.send({status:'failure',message:error.message});
            });
}


router.get('/',async function(req, res, next) {	
        var id    = req.query.class_id;
        var name  = req.query.class_name;
        var user_id =  req.query.user_id;
        var depName = req.query.departmentName;
        var getConnection = require("../../dbConnections/getConnection");
        const db = getConnection(depName);
        
        var isLoggedIn = await checkLoggedIn(db,req,res);
        var hasPermission = await checkHasPermission(db,user_id,3,res); //check if the user has write or update permission

        console.log("isLoggedIn = ",isLoggedIn);
        if(!isLoggedIn){
            res.send({status:'failure',message:'Unauthorized Access'});
        }

        if(!hasPermission){
            res.send({status:'failure',message:'Permission Denied'});
        }
        console.log(id,name);
        await modifyClass(db,name,id,res);
        res.send({status:'success',message:"Moidfy class Succesfully"});
});

module.exports = router;