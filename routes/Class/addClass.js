'use strict';
var express = require('express');
var router = express.Router();
var checkLoggedIn = require('../Authentication/CheckIsLoggedIn');
var checkHasPermission = require('../Authentication/checkHasPermission');
var getConnection = require("../../dbConnections/getConnection");

async function getID(db,res){
    
    return db.any('SELECT CLASS_ID FROM CLASS ORDER BY CLASS_ID DESC LIMIT 1', [true])
    .then(function(data) {
        // success;
        return data.length>0?(parseInt(data[0].class_id)+1):1;
    })
    .catch(function(error) {
        // error;
        console.log(error);
        res.send({status:'error',message:'error.message'});
    });
}


router.get('/',async function(req, res, next) {
    var name   =    req.query.name;
    var user_id =  req.query.user_id;
    var depName = req.query.departmentName;
    var {class_strength} = req.query;
    console.log("Department Name = ",depName);
    const db = getConnection(depName);
    var new_id =    await getID(db,res);

    if(name===null || name===undefined){
        res.send({status:'error',message:'Kindly Enter the Class Name'});
        return;
    }


    if(class_strength===null || class_strength===undefined){
        res.send({status:"error",message:'Kindly enter class strength'});
        return;
    }

    var isLoggedIn = await checkLoggedIn(db,req,res);
    var hasPermission = await checkHasPermission(db,user_id,4,res); //check if the user has write or create permission

    console.log("isLoggedIn = ",isLoggedIn);
    if(!isLoggedIn){
        res.send({status:'failure',message:'Unauthorized Access'});
        return;
    }

    if(!hasPermission){
        res.send({status:'failure',message:'Permission Denied'});
        return;
    }

    console.log(new_id);
    return db.one('INSERT INTO CLASS(CLASS_ID,CLASS_NAME,NO_OF_SUBJECTS,NO_OF_HOURS_PER_WEEK,CLASS_STRENGTH) VALUES($1, $2, $3, $4,$5) RETURNING CLASS_ID', [new_id,name,0,0,class_strength])
    .then(data => {
        console.log("New Class Added with id ",data.id); // print new user id;
        res.send({status:'success',message:'Class Added Succesfully'});
    })
    .catch(error => {
        console.log('ERROR:', error.message); // print error;
        res.send({status:'error',message:error.message});
    })
            
});
  
module.exports = router;