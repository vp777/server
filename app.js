/*

COPYRIGHT 2019,VINAY P, ALL RIGHTS RESERVED

*/

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
//var os = require('./host_details'); //to initialize
var nothing = require("./dbConnections/index"); // to initalize
var pgsqlconn = require('./db'); //to initialize
var cors = require('cors');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var addClassRouter    = require('./routes/Class/addClass');
var removeClass       = require('./routes/Class/removeClass');
var modifyClass       = require('./routes/Class/modifyClass');

var addTeacher        = require('./routes/Teacher/addTeacher');
var removeTeacher     = require('./routes/Teacher/removeTeacher');
var editTeacher       = require('./routes/Teacher/editTeacher');
var getTeacherSubjects= require("./routes/Teacher/getTeacherSubjects");

var addRoom           = require('./routes/Room/addRoom');
var removeRoom        = require('./routes/Room/deleteRoom');

var addSubject        = require('./routes/Subject/addSubject');
var removeSubject     = require('./routes/Subject/removeSubject');

var getData           = require('./routes/getData');

var generateTimeTable = require('./routes/GenerateTimeTable/GenTimeTable');

var loginRouter       = require('./routes/Authentication/login');

var getTimeTable      = require('./routes/GetTimeTable/getTimeTable');
var getTeachTimeTable = require('./routes/GetTimeTable/getTeacherTimeTable');

var addUser           = require('./routes/User/addUser');
var delUser           = require('./routes/User/deleteUser');
var getUser           = require('./routes/User/getUser');

var getMisc           = require("./routes/Teacher/getMisc");
var updateMisc        = require("./routes/Teacher/updateMisc");

var addTeacherLeave   = require("./routes/TeacherLeaves/addTeacherLeave");
var getTeacherLeaves  = require("./routes/TeacherLeaves/getTeacherLeaves");

var getMiscStartDate  = require("./routes/miscData/getStartDate");
var updateStartDate   = require("./routes/miscData/updateStartDate");

var app = express();

app.use(cors());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use("/addTeacherLeave",addTeacherLeave);
app.use("/getTeacherLeaves",getTeacherLeaves);

app.use("/getMisc",getMisc);
app.use("/updateMisc",updateMisc);

app.use("/getMiscStartDate",getMiscStartDate);

app.use('/getData',getData);

app.use('/addClass',addClassRouter);
app.use('/removeClass',removeClass);
app.use('/modifyClass',modifyClass);

app.use('/addTeacher',addTeacher);
app.use('/removeTeacher',removeTeacher);
app.use('/editTeacher',editTeacher);
app.use("/getTeacherSubjects",getTeacherSubjects);

app.use('/addRoom',addRoom);
app.use('/removeRoom',removeRoom);

app.use('/addSubject',addSubject);
app.use('/removeSubject',removeSubject);

app.use('/gentimetable',generateTimeTable);

app.use('/login',loginRouter);

app.use('/getClassTimeTable',getTimeTable);

app.use('/getTeacherTimeTable',getTeachTimeTable);

app.use("/updateMiscStartDate",updateStartDate);

app.use('/addUser',addUser);
app.use('/delUser',delUser);
app.use('/getUser',getUser);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;



