var cadets_of_training_wing = require("./cadets_of_training_wing");
var faculty_of_aeronautics = require("./faculty_of_aeronautics");
var faculty_of_degree_engineering = require('./faculty_of_degree_engineering');
var faculty_of_electrical_and_mechnical_engineering = require("./faculty_of_electrical_and_mechanical_engineering")
var faculty_of_electronics = require("./faculty_of_electronics")
var faculty_of_equipment_manaagement_and_tactics = require("./faculty_of_equipment_management_and_tactics");

var nothing = "doesnt exist";

module.exports = {
        cadets_of_training_wing,
        faculty_of_aeronautics,
        faculty_of_degree_engineering,
        faculty_of_electrical_and_mechnical_engineering,
        faculty_of_electronics,
        faculty_of_equipment_manaagement_and_tactics,
        nothing
    };