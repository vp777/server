const departmentNames = [ 
    "Faculty of Degree Engineering",
    "Faculty of Aeronautics",
    "Faculty of Electronics",
    "Cadets Training Wing",
    "Faculty of Equipment Management and Tactics",
    "Faculty of Electrical and Mechanical Engineering",
];

const faculty_of_degree_engineering = require("./faculty_of_degree_engineering");
const faculty_of_electronics = require("./faculty_of_electronics");
const faculty_of_equipment_management_and_tactics = require("./faculty_of_equipment_management_and_tactics");
const faculty_of_electrical_and_mechnical_engineering  = require("./faculty_of_electrical_and_mechanical_engineering");
const cadets_of_training_wing = require("./cadets_of_training_wing");
const faculty_of_aeronautics = require("./faculty_of_aeronautics");

const getConnection = (departmentName) =>{
    switch(departmentName){
        case departmentNames[0]:
            return faculty_of_degree_engineering;
        case departmentNames[1]:
            return faculty_of_aeronautics;
        case departmentNames[2]:
            return faculty_of_electronics;
        case departmentNames[3]:
            return cadets_of_training_wing;
        case departmentName[4]:
            return faculty_of_equipment_management_and_tactics;
        case departmentName[5]:
            return faculty_of_electrical_and_mechnical_engineering;
        default:
            console.log("Not found any");
            return null;
    }
}

module.exports = getConnection;
